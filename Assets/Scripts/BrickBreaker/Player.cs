﻿using UnityEngine;

public class Player : MonoBehaviour
{
	private const string HORIZONTAL = "Horizontal";
	[SerializeField] private Rigidbody _rigidbody;
	[SerializeField] private float _speed = 10;

	private float _horizontalMovement;
	private void Update()
	{
		_horizontalMovement = Input.GetAxis(HORIZONTAL);
	}

	private void FixedUpdate()
	{
		//_rigidbody.AddForce(transform.right * _horizontalMovement * Time.fixedDeltaTime * _speed, ForceMode.VelocityChange); //speed: 35
		_rigidbody.velocity = transform.right * _horizontalMovement * _speed * Time.fixedDeltaTime; //speed: 400
	}
}
