﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelMaster : MonoBehaviour
{
	private const string FIRE1 = "Fire1";
	[SerializeField] private Ball _ball;
	[SerializeField] private int _playerHealth;
	[SerializeField] private GameObject _gameOver;
	[SerializeField] private Brick[] _bricks;
	[SerializeField] private Text _pointText; 

	private Vector3 _ballStartPosition;
	private int _currentPoints;

	private void Awake()
	{
		_ballStartPosition = _ball.transform.position;
		_ball.Init(this);
		foreach (var currBrick in _bricks)
			currBrick.Init(this);
	}

	private void Update()
	{
		if (Input.GetButtonDown(FIRE1))
		{
			if (_gameOver.activeSelf)
				GameRestart();
			else
				_ball.Fire();
		}
	}

	private void GameRestart()
	{
		Debug.Log("GameRestart");
		SceneManager.LoadScene(0);
	}

	public void OnBallDied(Ball ball)
	{
		--_playerHealth;
		if (_playerHealth > 0)
			ball.DoReset(_ballStartPosition);
		else
			PlayerDie();
	}
	private void PlayerDie()
	{
		Debug.Log("Halott vagy");
		_gameOver.SetActive(true);
	}

	public void OnBrickDied(int brickMaxHealth)
	{
		_currentPoints += brickMaxHealth * 100;
		Debug.Log("CurrentPoint: " + _currentPoints);
		//_pointText.text = Convert.ToString( _currentPoints);
		//_pointText.text = _currentPoints + "";
		_pointText.text = _currentPoints.ToString();
		_pointText.color = UnityEngine.Random.ColorHSV();
	}
}
