﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
	[SerializeField] [Range(1, 3)] private int _health = 1;
	[SerializeField] private GameObject[] _models;

	private LevelMaster _levelMaster;
	private int _maxHealth;

	private void Awake()
	{
		ChangeModelByHealth();
		_maxHealth = _health;
	}

	public void Init(LevelMaster levelMaster)
	{
		_levelMaster = levelMaster;
	}

	private void OnCollisionEnter(Collision collision)
	{
		var ball = collision.gameObject.GetComponent<Ball>();
		if (ball != null)
		{
			TakeDamage();
		}
	}

	private void TakeDamage()
	{
		--_health;
		if (_health <= 0)
		{
			Die();
		}
		else
		{
			ChangeModelByHealth();
		}
	}

	private void ChangeModelByHealth()
	{
		foreach (var currModel in _models)
		{
			currModel.SetActive(false);
		}
		_models[_health - 1].SetActive(true);
	}

	private void Die()
	{
		gameObject.SetActive(false);
		_levelMaster.OnBrickDied(_maxHealth);
		//Destroy(gameObject);
	}
}
