﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
	[SerializeField] private Rigidbody _rigidbody;
	[SerializeField] private float _speed;
	[SerializeField] private float _maxSpeed;
	[SerializeField] private float _acceleration = 1.05f;

	private bool _isFired = false;
	private LevelMaster _levelMaster;
	private Vector3 _lastVelocity;
	private float _minSpeed;

	private void Awake()
	{
		_minSpeed = _speed;
	}

	public void Init(LevelMaster levelMaster)
	{
		_levelMaster = levelMaster;
	}

	public void Fire()
	{
		if (_isFired)
			return;
		_rigidbody.AddForce(Vector3.down * _speed, ForceMode.VelocityChange);
		_isFired = true;
	}

	public void DoReset(Vector3 ballStartPosition)
	{
		transform.position = ballStartPosition;
		_isFired = false;
		_rigidbody.velocity = Vector3.zero;
	}

	private void FixedUpdate()
	{
		_lastVelocity = _rigidbody.velocity;
	}

	private void OnTriggerEnter(Collider other)
	{
		var killZone = other.gameObject.GetComponent<KillZone>();
		if (killZone)
		{
			_levelMaster.OnBallDied(this);
		}
	}

	private void OnCollisionEnter(Collision collision)
	{
		Vector3 outVector = ReflectBallMovementOnContact(collision.contacts[0]);
		outVector = AcceleratBallVelocity(outVector);
		_rigidbody.velocity = outVector;
	}

	private Vector3 AcceleratBallVelocity(Vector3 outVector)
	{
		outVector *= _acceleration;
		var magnitude = outVector.magnitude;
		if (magnitude > _maxSpeed)
		{
			outVector = outVector.normalized * _maxSpeed;
		}
		else if (magnitude < _minSpeed)
		{
			outVector = outVector.normalized * _minSpeed;
		}
		//var limitedMagintude = Mathf.Min(magnitude, _maxSpeed);
		//limitedMagintude = Mathf.Max(limitedMagintude, _minSpeed);
		return outVector;
	}

	private Vector3 ReflectBallMovementOnContact(ContactPoint contact)
	{
		return Vector3.Reflect(_lastVelocity, contact.normal);
	}

	//ContactPoint _contact;
	//Vector3 _lastVelocity;
	//private void Update()
	//{
	//	Debug.DrawRay(_contact.point, _contact.normal * 100, Color.red);
	//	//Debug.DrawRay(transform.position, _rigidbody.velocity * 100, Color.green);
	//}

	//private void FixedUpdate()
	//{
	//	_lastVelocity = _rigidbody.velocity;
	//}

	//public void OnCollisionEnter(Collision collision)
	//{
	//	_contact = collision.contacts[0];
	//	Quaternion rot = Quaternion.FromToRotation(Vector3.up, _contact.normal);
	//	Debug.Log(_rigidbody.velocity);
	//	Debug.DrawRay(transform.position, _lastVelocity * 100, Color.magenta);
	//	var outVector = Vector3.Reflect(_lastVelocity, _contact.normal);
	//	Debug.DrawRay(_contact.point, outVector, Color.yellow);
	//	//Debug.Log(rot);
	//	//Debug.Log(rot.eulerAngles);
	//	Debug.Break();
	//}
}
