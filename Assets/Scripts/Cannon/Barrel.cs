﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrel : MonoBehaviour, IDamageable
{
	[SerializeField] private float _health;
	[SerializeField] private GameObject _epicDestoryEffect;
	[SerializeField] private GameObject _model;
	public float Health
	{
		get => _health;
		set
		{
			_health = value;
			if(_health <= 0.0f)
			{
				Die();
			}
		}
	}

	public void TakeDamage(float damage)
	{
		Health -= damage;
	}
	private void Die()
	{
		_model.SetActive(false);
		Instantiate(_epicDestoryEffect, transform.position, transform.rotation);
		StartSlowMotion();
	}

	private void StartSlowMotion()
	{
		SetTimeScale(0.01f);
		Invoke(nameof(EndSlowMotion), 5.0f * Time.timeScale);
	}

	private void EndSlowMotion()
	{
		SetTimeScale(1f);
		Destroy(gameObject);
	}
	private void SetTimeScale(float timeScale)
	{
		Time.timeScale = timeScale;
		Time.fixedDeltaTime = Time.timeScale * 0.02f;
	}
}
