﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
	[SerializeField] private CannonBall _cannonBallPrefab;
	[SerializeField] private Transform _launchPosition;
	[SerializeField] private float _fireForce = 10;

    void Update()
    {
		if(Input.GetKeyDown(KeyCode.Space))
		{
			Fire();
		}
    }

	private void Fire()
	{
		var cannonBall = Instantiate(_cannonBallPrefab, _launchPosition.position, _launchPosition.rotation);
		cannonBall.Shot(_fireForce);
	}
}
