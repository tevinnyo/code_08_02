﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour
{
	[SerializeField] private Rigidbody _rigidbody;
	[SerializeField] private float _lifeTime = 5.0f; 

	private void Start()
	{
		Invoke(nameof(DestroyGO), _lifeTime);
	}

	public void Shot( float force)
	{
		_rigidbody.AddForce(transform.forward * force, ForceMode.Impulse);
	}

	private void DestroyGO()
	{
		Destroy(gameObject);
	}

	private void OnCollisionEnter(Collision collision)
	{
		var damageable = collision.gameObject.GetComponent<IDamageable>();
		//damageable?.TakeDamage(25.0f);
		if(damageable != null)
			damageable.TakeDamage(25.0f);
	}
}
