﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eTag
{
	Player,
	Enemy,
	Tree
}

public class FunctionTest : MonoBehaviour, IDamageable
{
	[SerializeField] private eTag _tag;
	[SerializeField] private Rigidbody _rigidbody;
	[SerializeField] private ChildScript[] _childScripts;

	private void Awake()
	{
		Debug.Log("Awake");
		Debug.Log("enabled: " + enabled);
		Debug.Log("gameObject.activeSelf: " + gameObject.activeSelf);
		if (!_rigidbody)
			_rigidbody = gameObject.GetComponent<Rigidbody>();
		var box = gameObject.GetComponent<BoxCollider>();
		var boxes = gameObject.GetComponents<BoxCollider>();
		var damageable = GetComponent<IDamageable>();
		//gameObject.transform.GetChild(0).GetComponent<ChildScript>().PrintName();
		_childScripts[0].PrintName();
	}

	private void Start()
	{
		Debug.Log(gameObject);
		Debug.Log(transform);
		Debug.Log(gameObject.transform); //ugyanaz mint az elozo
		Debug.Log(name);
		Debug.Log(gameObject.name); //ugyanaz mint az elozo
		Debug.Log(tag);
		Debug.Log(gameObject.tag); //ugyanaz mint az elozo
		tag = "Finish";
		if(tag ==  "Player")
		{

		}
		if(_tag == eTag.Player)
		{

		}
		Debug.Log("enabled: " + enabled);
		Debug.Log("gameObject.activeSelf: " + gameObject.activeSelf);
		Destroy(gameObject);
	}

	private void Update()
	{
		//Debug.Log("Update");
	}

	private void FixedUpdate()
	{
		//Debug.LogError("FixedUpdate");
	}

	public void TakeDamage(float damage)
	{
		Debug.Log("AU EZ FAJT: " + damage);
	}
}

