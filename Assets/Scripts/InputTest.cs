﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputTest : MonoBehaviour
{
	const string HORIZONTAL = "Horizontal";
	const string VERTICAL = "Vertical";
	void Update()
	{
		float buttonValue = Input.GetAxis(HORIZONTAL);
		if (buttonValue != 0)
		{
			Debug.LogError("Horizontal: " + buttonValue);
		}
		buttonValue = Input.GetAxis("Fire1");
		if (buttonValue != 0)
		{
			Debug.LogError("Fire: " + buttonValue);
		}
	}
}
