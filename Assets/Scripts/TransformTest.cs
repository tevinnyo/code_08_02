﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class TransformTest : MonoBehaviour
{
	[SerializeField] private float _speed = 1;
	[SerializeField] private Rigidbody _rigidbody;
	[SerializeField] private ForceMode _forceMode;

	Vector3 _startPosition;
	public float _timer = 0;
	void Start()
    {
		_startPosition = transform.position;
		//transform.position = new Vector3(10, 0, 0);
		//Time.timeScale = 0.001f; //BulletTime -> 1000%os lassitas
		//Time.fixedDeltaTime = Time.timeScale * 0.02f; //Fizikaszamitast is modositani kell hozza
		//_rigidbody.AddForce(transform.up * _speed, _forceMode);
		//var nextPosition = Vector3.Lerp(Vector3.zero, Vector3.up, 0);
		//Debug.Log(nextPosition);
		//nextPosition = Vector3.Lerp(Vector3.zero, Vector3.up, 0.25f);
		//Debug.Log(nextPosition);
		//nextPosition = Vector3.Lerp(Vector3.zero, Vector3.up, 1);
		//Debug.Log(nextPosition);
	}

    void FixedUpdate()
    {
		//transform.position += new Vector3(1f, 0, 1f) * Time.fixedDeltaTime;
		//	transform.position = Vector3.MoveTowards(transform.position, new Vector3(10.0f, 0.0f, 0.0f), _speed * Time.fixedDeltaTime);
		//_rigidbody.MovePosition(new Vector3(10, 0, 0));
		var ratio = _timer / 10.0f;
		//_rigidbody.MovePosition(Vector3.Lerp(_startPosition, new Vector3(10, 0, 0), ratio));
		//_rigidbody.MovePosition(Vector3.MoveTowards(transform.position, new Vector3(10.0f, 0.0f, 0.0f), _speed * Time.fixedDeltaTime));
		_timer += Time.fixedDeltaTime;
	}
}
