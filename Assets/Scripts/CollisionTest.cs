﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionTest : MonoBehaviour
{

	private void OnCollisionEnter(Collision collision)
	{
		Debug.LogError("OnCollisionEnter");
	}
	private void OnCollisionStay(Collision collision)
	{
		Debug.Log("OnCollisionStay");

	}
	private void OnCollisionExit(Collision collision)
	{
		Debug.LogError("OnCollisionExit");

	}

	private void OnTriggerEnter(Collider other)
	{
		Debug.LogError("OnTriggerEnter");

	}
	private void OnTriggerStay(Collider other)
	{
		Debug.Log("OnTriggerStay");

	}
	private void OnTriggerExit(Collider other)
	{
		Debug.LogError("OnTriggerExit");

	}
}
